# Tyra - Un jeu avec GodotEngine en 2D

Le but est simple : survivre en faisant le meilleur score, mais attention, des ennemis venu d'une dimension inca vont t'en empêcher (parfois ils se font aider par Inti Illapa, le dieu du ciel, ce qui leur permet de traverser les murs à une vitesse incommensurable)

## Comment jouer ?
- Déplacement : Z / Q / S / D ou les flèches directionnelles
- Tir : Clic gauche de la souris
- Visée : Curseur de la souris

## Téléchargement
La dernière version se trouve sur la [page des releases](https://code.up8.edu/Anri/godot-2e-semestre/-/releases). Le jeu est disponible sur Windows / Linux et MacOS X. Le jeu est aussi directement compilable depuis [la branche master](https://code.up8.edu/Anri/godot-2e-semestre/-/tree/master) avec Godot (version précisé plus bas).

### Maps
Pour une meilleur compréhension des maps pour les joueurs débutant, des images des maps complètes sont stockés dans [le dossier maps](https://code.up8.edu/Anri/godot-2e-semestre/-/tree/master/maps).

### Crédit
- Le jeu est développpé sur Godot Engine version 3.2.3
- Les images ont été faîtes sur Gimp version 2.10.24
- Les sons ont été fait sur le site BeepBox au mois avril 2021 (cf. readme pour les liens correspondants)
- Les polices ont été faites sur le site FontStruct au mois d'avril 2021
- *Dans chaque dossier assets, il y a un readme pour les sources qui rappelle cela*

### Licence
Le jeu n'est sous aucune licence et a été developpé dans le cadre du cours de programmation sur les moteurs de jeux de Mr. Revault d'Allonnes.
