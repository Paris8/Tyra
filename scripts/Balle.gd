extends Area2D

var vitesseBalle = 700 # vitesse de la balle

var sonMortEnnemi = preload("res://scenes/SonMortEnnemi.tscn") # récupération Son

func _ready():
	pass

func _physics_process(delta): # déplacement de la balle
	position += transform.x * vitesseBalle * delta

func _on_Area2D_body_entered(body):
	if "Murs" in body.name: # supprime la balle quand elle percute un mur
		queue_free()
	if "Ennemi" in body.name: # quand le joueur touche qq1
		queue_free() # supprime l'ennemi
		var son = sonMortEnnemi.instance() # création object son
		son.position = get_global_position() # récupération de la position
		get_tree().get_root().call_deferred("add_child", son) # ajout du son
		global.ennemisTues += 1 # ajoute un ennemi tuer au compteur
		body.queue_free() # supprime aussi la balle
