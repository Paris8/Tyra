extends Control

func _ready():
	get_node("labelNiveauMax").set_text(str(global.monde)) # affichage niveau découvert
	get_node("labelEnnemisTues").set_text(str(global.ennemisTues)) # affichage ennemis tués
	var timer = Timer.new() # nouveau timer
	timer.connect("timeout", self, "_on_timer_timeout") # création timer
	timer.set_wait_time(2) # timer de 2 secondes
	add_child(timer) # Ajout timer à la scène
	timer.start() # Lance le timer

func _on_timer_timeout(): # à la fin du timer
	if get_tree().change_scene("res://scenes/Menu.tscn") != OK: # recommence au début
		print("Une erreur est survenue lors du rechargement de la scène.") # gestion erreur
