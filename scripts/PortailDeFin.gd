extends Area2D

func _ready():
	global.monde += 1 # ajoute 1 au monde que le joueur découvre quand le portail de fin est généré

func _on_Portail_de_fin_body_entered(body):
	if "Joueur" in body.name: # si la personne qui entre est un joueur
		var prochainMonde = "res://scenes/Monde%s.tscn" % global.randomMap() # prochaine map
		if get_tree().change_scene(prochainMonde) != OK: # go au menu
			print("Impossible de charger le jeu.") # gestion erreur
