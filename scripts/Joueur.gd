extends KinematicBody2D

var vitesseDeplacement = 500
var balle = preload("res://scenes/Balle.tscn") # précharge la balle

func _ready():
	pass

func _process(_delta):
	var deplacement = Vector2() # déplacement du personnage

	if Input.is_action_pressed("haut"): # vers le haut
		deplacement.y -= 1
	if Input.is_action_pressed("bas"): # vers le bas
		deplacement.y += 1
	if Input.is_action_pressed("gauche"): # vers la gauche
		deplacement.x -= 1
	if Input.is_action_pressed("droite"): # vers la droite
		deplacement.x += 1

	deplacement = deplacement.normalized() # empeche de doubler la vitesse en diagonale
	deplacement = move_and_slide(deplacement * vitesseDeplacement) # vitesse
	look_at(get_global_mouse_position()) # regarde le curseur

	if Input.is_action_just_pressed("clicGauche"): # tir
		tir()

func tir():
	var objectBalle = balle.instance() # création object balle
	objectBalle.position = get_global_position() # récupération de la position
	objectBalle.rotation_degrees = rotation_degrees # récupération du degré de rotation
	get_tree().get_root().call_deferred("add_child", objectBalle) # ajout de la balle

func mort():
	if get_tree().change_scene("res://scenes/GameOver.tscn") != OK: # écran game over
		print("Une erreur est survenue lors du rechargement de la scène.") # gestion erreur

func _on_Area2D_body_entered(body):
	if "Ennemi" in body.name: # collision avec l'ennemi
		mort() # on perd
