extends Node2D

func _ready(): # focus par défaut le bouton JOUER
	$MarginContainer/VBoxContainer/VBoxContainer/Jouer.grab_focus()
	global.monde = 0
	global.ennemisTues = 0

func _physics_process(_delta): # gestion de la souris
	if $MarginContainer/VBoxContainer/VBoxContainer/Jouer.is_hovered() == true:
		$MarginContainer/VBoxContainer/VBoxContainer/Jouer.grab_focus()
	if $MarginContainer/VBoxContainer/VBoxContainer/Quitter.is_hovered() == true:
		$MarginContainer/VBoxContainer/VBoxContainer/Quitter.grab_focus()

func _on_Jouer_pressed():
	if get_tree().change_scene("res://scenes/Monde%s.tscn" % global.randomMap()) != OK: # on charge le monde
		print("Impossible de charger le jeu.") # si le monde n'a pas chargé

func _on_Quitter_pressed():
	get_tree().quit() # quitte le jeu
