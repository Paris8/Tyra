extends KinematicBody2D

var deplacement = Vector2() # deplacement est un vecteur dans un plan 2d
var ecart = 270 # distance d'activation des mobs
var vitesse # init vitesse ennemi

# quand trop éloigné, quasi téléportation vers le joueur (aide de Inti Illapa)

func _ready():
	pass

func _physics_process(_delta):
	var Joueur = get_parent().get_parent().get_node("Joueur") # récupère le joueur
	if (Joueur.position.x + ecart) > position.x and (Joueur.position.y + (ecart + 10)) > position.y:
		vitesse = 70 # se déplace vers le joueur
	else:
		vitesse = 10000 # se déplace lentement, pas immobile, plus sympa
	position += (Joueur.position - position) / vitesse # déplacement

	var _resultMoveAndCollide = move_and_collide(deplacement) # empeche la superposition des ennemis + déplacement
