extends Node

# variable globales
var monde = 0
var ennemisTues = 0

"""
l'idée de l'aléatoire ici est de récupérer une map aléatoirement entre les
maps déja faite
"""
func randomMap(): # fonction pour définir le prochain monde
	var aleatoire = RandomNumberGenerator.new() # initialisation aléatoire
	aleatoire.randomize() # génération aléatoire
	if global.monde >= 10: # si au moins au 10e monde ----- mode "accomplie"
		return aleatoire.randi_range(1, 10) # tous les mondes selectionnés (1 à 10)
	elif global.monde >= 7: # si au moins au 7e monde ----- mode "expert"
		return aleatoire.randi_range(7, 10) # seulement les mondes de 7 à 10
	elif global.monde >= 5: # si au moins au 5e monde ----- mode "intermediaire"
		return aleatoire.randi_range(5, 7) # seulement les mondes de 5 à 7
	elif global.monde >= 3: # si au moins au 5e monde ----- mode "connaisseur"
		return aleatoire.randi_range(3, 5) # seulement les mondes de 3 à 5
	else: # moins de 2 mondes réussi ----- mode "débutant"
		return aleatoire.randi_range(1, 3) # seulement les mondes de 1 à 3
